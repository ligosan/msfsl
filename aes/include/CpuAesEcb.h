#include <cstdint>
#include <cstring>
#include <cstdio>
#include <thread>
#include <vector>

#include <x86intrin.h>

namespace msfsl
{
namespace Cpu
{

size_t getRounds( size_t key_size );

size_t getExpandedSize( size_t key_size );

void parseKeySize( size_t key_size );

void expandKeysEncrypt( uint8_t* out_keys, uint8_t* in_key, size_t key_size );
void expandKeysDecrypt( uint8_t* out_keys, uint8_t* in_key, size_t key_size );
void ecbEncrypt( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size );
void ecbDecrypt( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size );

/**
 * @ecbEncryptx86
 * 
 * Encrypts the given plain text using intel's AES-NI intrinsic functions.
 * 
 * @param cipher_text The cipher text to store the encrypted plain text
 * @param plain_text Input text to encrypted
 * @param keys Expanded keys
 * @param text_size Length of the plain text in bytes
 * @param key_size Length of the key in bytes
 */
void ecbEncryptx86( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size );

void ecbEncryptx86Multi( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size );

void ecbDecryptx86( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size );

void ecbDecryptx86Multi( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size );
}
}