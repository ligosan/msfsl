#define bitslicekey0( key, bskey ) \
    xmm0 = *( __int128_t* ) ( key + 0 )

#define uint128_t __uint128_t
#define ONE 0xffffffff
#define BS0 0x55555555555555555555555555555555
#define BS1 0x33333333333333333333333333333333
#define BS2 0x0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f
#define M0 0x02060a0e03070b0f0004080c0105090d

typedef struct
{
    uint32_t bs_key[11][32];
    uint32_t counter[4];
} __attribute__ ((aligned (16))) ECRYPT_ctx;

#define bitslicekey(i,key,bskey) \
  xmm0 = *(uint128_t *) (key + 0);\
  shuffle bytes of xmm0 by M0;\
  xmm1 = xmm0;\
  xmm2 = xmm0;\
  xmm3 = xmm0;\
  xmm4 = xmm0;\
  xmm5 = xmm0;\
  xmm6 = xmm0;\
  xmm7 = xmm0;\
  ;\
  bitslice(xmm7, xmm6, xmm5, xmm4, xmm3, xmm2, xmm1, xmm0, t);\
  ;\
  xmm6 ^= ONE;\
  xmm5 ^= ONE;\
  xmm1 ^= ONE;\
  xmm0 ^= ONE;\
  ;\
  *(uint128_t *) (bskey + 0 + 128 * i ) = xmm0;\
  *(uint128_t *) (bskey + 16 + 128 * i ) = xmm1;\
  *(uint128_t *) (bskey + 32 + 128 * i ) = xmm2;\
  *(uint128_t *) (bskey + 48 + 128 * i ) = xmm3;\
  *(uint128_t *) (bskey + 64 + 128 * i ) = xmm4;\
  *(uint128_t *) (bskey + 80 + 128 * i ) = xmm5;\
  *(uint128_t *) (bskey + 96 + 128 * i ) = xmm6;\
  *(uint128_t *) (bskey + 112 + 128 * i ) = xmm7;\

#define bitslicekey0( key, bskey )\
xmm0 = *( uint128_t* ) ( key + 0 )\
/* TODO shuffle bytes of xmm0 by M0; */ \
xmm1 = xmm0;\
xmm2 = xmm0;\
xmm3 = xmm0;\
xmm4 = xmm0;\
xmm5 = xmm0;\
xmm6 = xmm0;\
xmm7 = xmm0;\
;\
bitslice(xmm7, xmm6, xmm5, xmm4, xmm3, xmm2, xmm1, xmm0, t);\
;\
*(uint128_t*) (bskey + 0) = xmm0;\
*(uint128_t*) (bskey + 16) = xmm1;\
*(uint128_t*) (bskey + 32) = xmm2;\
*(uint128_t*) (bskey + 48) = xmm3;\
*(uint128_t*) (bskey + 64) = xmm4;\
*(uint128_t*) (bskey + 80) = xmm5;\
*(uint128_t*) (bskey + 96) = xmm6;\
*(uint128_t*) (bskey + 112) = xmm7;\

#define swapmove(a, b, n, m, t) \	
	t = b;\
	t >>= n;\
	t ^= a;\
	t &= m;\
	a ^= t;\
	t <<= n;\
	b ^= t;\

#define bitslice( x0, x1, x2, x3, x4, x5, x6, x7, t ) \
    swapmove( x0, x1, 1, BS0, t );                    \
    swapmove( x2, x3, 1, BS0, t );                    \
    swapmove( x4, x5, 1, BS0, t );                    \
    swapmove( x6, x7, 1, BS0, t );                    \
    ;                                                 \
    swapmove( x0, x2, 2, BS1, t );                    \
    swapmove( x1, x3, 2, BS1, t );                    \
    swapmove( x4, x6, 2, BS1, t );                    \
    swapmove( x5, x7, 2, BS1, t );                    \
    ;                                                 \
    swapmove( x0, x4, 4, BS2, t );                    \
    swapmove( x1, x5, 4, BS2, t );                    \
    swapmove( x2, x6, 4, BS2, t );                    \
    swapmove( x3, x7, 4, BS2, t );                    \
    \
