#include <cstdint>
#include <cstring>
#include <cstdio>
#include <thread>
#include <vector>

#include <x86intrin.h>

namespace msfsl
{
namespace Cpu
{
void ctrCryptx86( uint8_t* out_text, const uint8_t* in_text, uint8_t* keys, const std::vector<uint8_t> iv, const size_t text_size, const size_t key_size );
void ctrCryptx86Multi( uint8_t* out_text, uint8_t* in_text, uint8_t* keys, const std::vector<uint8_t> iv, size_t text_size, size_t key_size );
}
}