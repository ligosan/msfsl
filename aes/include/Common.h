#ifndef COMMON_H
#define COMMON_H

#include <x86intrin.h>

inline size_t getRounds( size_t key_size )
{
    size_t return_size;
    switch ( key_size )
    {
    case 16:
    case 128:
        return_size = 10;
        break;
    case 24:
    case 192:
        return_size = 12;
        break;
    case 32:
    case 256:
        return_size = 14;
        break;
    }
    return return_size;
}

inline __m128i* loadKeysx86( uint8_t* keys )
{
    __m128i* k;

#ifdef __linux__
    k = ( __m128i* ) keys;
#elif __WIN32__
    __m128i k[ rounds + 1 ];
    for ( int i = 0; i < rounds + 1; i++ )
    {
        k[ i ] = _mm_loadu_si128( ( __m128i* ) keys );
        keys += AES_BLOCK_SIZE;
    }
#endif

    return k;
}

inline void aesBlockEncryptx86( uint8_t* out_text, uint8_t* in_text, __m128i* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );

    __m128i m = _mm_loadu_si128( ( __m128i* ) in_text );

    m = _mm_xor_si128( m, keys[ 0 ] );

    for ( size_t i = 1; i < rounds; i++ )
    {
        m = _mm_aesenc_si128( m, keys[ i ] );
    }

    m = _mm_aesenclast_si128( m, keys[ rounds ] );
    _mm_storeu_si128( ( __m128i* ) out_text, m );
}

inline void aesBlockDecryptx86( uint8_t* out_text, uint8_t* in_text, __m128i* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );

    __m128i m = _mm_loadu_si128( ( __m128i* ) in_text );

    m = _mm_xor_si128( m, keys[ 0 ] );

    for ( size_t i = 1; i < rounds; i++ )
    {
        m = _mm_aesdec_si128( m, keys[ i ] );
    }

    m = _mm_aesdeclast_si128( m, keys[ rounds ] );
    _mm_storeu_si128( ( __m128i* ) out_text, m );
}

#endif