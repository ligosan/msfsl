#!/bin/bash

clang-format-3.6 -style=file -i $(find -name "*.cpp" -o -name "*.h" -o -name "*.cu" -o -name "*.cc")
