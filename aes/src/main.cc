#include <iostream>
#include <fstream>
#include <vector>

#include "Aes.h"
#include "boost/program_options.hpp"

using namespace std;

#define MAX_FILE_SIZE 1 * 1024 * 1024

void aesCtrCrypt( std::vector<uint8_t>& input_data )
{
    try
    {
        std::string key = "1234567890123456";
        std::string ivs = "1234567890123456";
        std::vector<uint8_t> expanded_keys( 176 );

        std::vector<uint8_t> iv( ivs.begin(), ivs.end() );

        msfsl::Cpu::expandKeysEncrypt( expanded_keys.data(), ( uint8_t* ) key.data(), key.size() );

        std::vector<uint8_t> out_text( input_data.size() );
        msfsl::Cpu::ctrCryptx86Multi( out_text.data(), input_data.data(), expanded_keys.data(), iv, input_data.size(), key.size() );
        input_data = std::vector<uint8_t>( out_text.begin(), out_text.end() );
    }
    catch ( const std::exception& e )
    {
        std::cerr << e.what() << '\n';
    }
}

void decrypt( std::vector<uint8_t>& input_data )
{
    aesCtrCrypt( input_data );
}

void encrypt( std::vector<uint8_t>& input_data )
{
    aesCtrCrypt( input_data );
}

std::vector<uint8_t> readFile( std::string file_name )
{
    // Read in file
    ifstream input_file( file_name, ios::in | ios::binary | ios::ate );
    streampos size = input_file.tellg();
    std::vector<uint8_t> input_blocks( size );
    input_blocks.reserve( size );

    input_file.seekg( 0, ios::beg );
    input_file.read( ( char* ) input_blocks.data(), size );

    input_file.close();

    return input_blocks;
}

int main( int argc, char* argv[] )
{
    try
    {
        boost::program_options::options_description desc( "Allowed Options" );
        desc.add_options()( "help,h", "produce help message" )( "encrypt,e", boost::program_options::value<std::string>(), "encrypts a file" )( "decrypt,d", boost::program_options::value<std::string>(), "decrypts a file" );

        boost::program_options::variables_map vm;
        boost::program_options::store( boost::program_options::parse_command_line( argc, argv, desc ), vm );
        boost::program_options::notify( vm );

        if ( vm.count( "help" ) )
        {
            cout << desc << "\n";
            return 0;
        }

        if ( vm.count( "encrypt" ) )
        {
            // cout << vm[ "encrypt" ].as< std::string >() << std::endl;

            std::string file_name( argv[ 2 ] );

            // Read in file
            auto input_blocks = readFile( file_name );

            cout << "Encrypting" << std::endl;
            encrypt( input_blocks );
            std::cout << "Encryption Done" << std::endl;

            ofstream output_file( file_name + ".msfsl", ios::out | ios::binary );
            output_file.write( ( char* ) input_blocks.data(), input_blocks.size() );
            output_file.close();

            std::remove( file_name.c_str() );

            return 0;
        }
        else if ( vm.count( "decrypt" ) )
        {
            std::string file_name( argv[ 2 ] );

            auto input_blocks = readFile( file_name );

            cout << "Decrypting" << std::endl;
            decrypt( input_blocks );
            std::cout << "Decryption Done" << std::endl;

            auto suffix = file_name.find( ".msfsl" );
            auto new_file_name = file_name.substr( 0, suffix );

            ofstream output_file2( new_file_name, ios::out | ios::binary );
            output_file2.write( ( char* ) input_blocks.data(), input_blocks.size() );
            output_file2.close();

            std::remove( file_name.c_str() );

            return 0;
        }
        else
        {
            cout << desc << "\n";
            return 0;
        }
    }
    catch ( const std::exception& e )
    {
        std::cerr << e.what() << '\n';
    }
}