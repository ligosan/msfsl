#include "CpuAesEcb.h"
#include <cassert>
#include <iostream>

#include "Common.h"

#define NUM_THREADS 4
#define AES_BLOCK_SIZE 16
#define CEIL_DIV( a, b ) ( ( a + b - 1 ) / b )

uint8_t sbox[ 256 ] = {
    0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
    0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
    0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
    0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
    0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
    0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
    0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
    0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
    0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
    0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
    0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
    0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
    0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
    0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
    0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
    0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};

uint8_t inverse_sbox[ 256 ] = {
    0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB,
    0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB,
    0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E,
    0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25,
    0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92,
    0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84,
    0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06,
    0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B,
    0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73,
    0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E,
    0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B,
    0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4,
    0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F,
    0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF,
    0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D
};

size_t msfsl::Cpu::getRounds( size_t key_size )
{
    size_t return_size;
    switch ( key_size )
    {
    case 16:
    case 128:
        return_size = 10;
        break;
    case 24:
    case 192:
        return_size = 12;
        break;
    case 32:
    case 256:
        return_size = 14;
        break;
    }
    return return_size;
}

size_t msfsl::Cpu::getExpandedSize( size_t key_size )
{
    size_t return_size;
    switch ( key_size )
    {
    case 16:
    case 128:
        return_size = 176;
        break;
    case 24:
    case 192:
        return_size = 204;
        break;
    case 32:
    case 256:
        return_size = 240;
        break;
    }
    return return_size;
}

void msfsl::Cpu::parseKeySize( size_t key_size )
{
    switch ( key_size )
    {
    case 16:
    case 128:
        key_size = 16;
        break;
    case 24:
    case 192:
        key_size = 24;
        break;
    case 32:
    case 256:
        key_size = 32;
        break;
    }
}

void rotWord( uint8_t* input )
{
    auto tmp_a = input[ 0 ];
    for ( size_t i = 0; i < 3; i++ )
    {
        input[ i ] = input[ i + 1 ];
    }
    input[ 3 ] = tmp_a;
}

void subBytes( uint8_t* state )
{
    for ( size_t i = 0; i < AES_BLOCK_SIZE; i++ )
    {
        state[ i ] = sbox[ state[ i ] ];
    }
}

void invSubBytes( uint8_t* state )
{
    for ( size_t i = 0; i < AES_BLOCK_SIZE; i++ )
    {
        state[ i ] = inverse_sbox[ state[ i ] ];
    }
}

// TODO Matrix is transposed!!!
void shiftRows( uint8_t* state )
{
    /*
  auto t = state[ 4 + 0 ];
  state[ 4 + 0 ] = state[ 4 + 1 ];
  state[ 4 + 1 ] = state[ 4 + 2 ];
  state[ 4 + 2 ] = state[ 4 + 3 ];
  state[ 4 + 3 ] = t;
  
  t = state[ 8 + 0 ];
  auto s = state[ 8 + 1 ];
  state[ 8 + 0 ] = state[ 8 + 2 ];
  state[ 8 + 1 ] = state[ 8 + 3 ];
  state[ 8 + 2 ] = t;
  state[ 8 + 3 ] = s;
  
  t = state[ 12 + 3 ];
  state[ 12 + 3 ] = state[ 12 + 2 ];
  state[ 12 + 2 ] = state[ 12 + 1 ];
  state[ 12 + 1 ] = state[ 12 + 0 ];
  state[ 12 + 0 ] = t;
	*/

    unsigned char tmp[ 16 ];

    tmp[ 0 ] = state[ 0 ];
    tmp[ 1 ] = state[ 5 ];
    tmp[ 2 ] = state[ 10 ];
    tmp[ 3 ] = state[ 15 ];

    tmp[ 4 ] = state[ 4 ];
    tmp[ 5 ] = state[ 9 ];
    tmp[ 6 ] = state[ 14 ];
    tmp[ 7 ] = state[ 3 ];

    tmp[ 8 ] = state[ 8 ];
    tmp[ 9 ] = state[ 13 ];
    tmp[ 10 ] = state[ 2 ];
    tmp[ 11 ] = state[ 7 ];

    tmp[ 12 ] = state[ 12 ];
    tmp[ 13 ] = state[ 1 ];
    tmp[ 14 ] = state[ 6 ];
    tmp[ 15 ] = state[ 11 ];

    memcpy( state, tmp, 16 );
}

void invShiftRows( uint8_t* state )
{
    unsigned char tmp[ 16 ];

    tmp[ 0 ] = state[ 0 ];
    tmp[ 1 ] = state[ 13 ];
    tmp[ 2 ] = state[ 10 ];
    tmp[ 3 ] = state[ 7 ];

    tmp[ 4 ] = state[ 4 ];
    tmp[ 5 ] = state[ 1 ];
    tmp[ 6 ] = state[ 14 ];
    tmp[ 7 ] = state[ 11 ];

    tmp[ 8 ] = state[ 8 ];
    tmp[ 9 ] = state[ 5 ];
    tmp[ 10 ] = state[ 2 ];
    tmp[ 11 ] = state[ 15 ];

    tmp[ 12 ] = state[ 12 ];
    tmp[ 13 ] = state[ 9 ];
    tmp[ 14 ] = state[ 6 ];
    tmp[ 15 ] = state[ 3 ];

    memcpy( state, tmp, 16 );
}

void addRoundKey( uint8_t* state, uint8_t* round_key )
{
    for ( size_t i = 0; i < AES_BLOCK_SIZE; i++ )
    {
        state[ i ] ^= round_key[ i ];
    }
}

void mixColumns( uint8_t* state )
{
    uint8_t state_a[ 4 ];
    uint8_t state_b[ 4 ];
    uint8_t h_bit = 0U;

    for ( size_t k = 0; k < 4; k++ )
    {
        for ( size_t i = 0; i < 4; i++ )
        {
            state_a[ i ] = state[ i + ( k * 4 ) ];
            h_bit = state[ i + ( k * 4 ) ] & 0x80;
            state_b[ i ] = state[ i + ( k * 4 ) ] << 1;
            if ( h_bit == 0x80 )
            {
                state_b[ i ] ^= 0x1b;
            }
        }

        state[ 0 + ( k * 4 ) ] = state_a[ 1 ] ^ state_a[ 2 ] ^ state_a[ 3 ] ^ state_b[ 0 ] ^ state_b[ 1 ];
        state[ 1 + ( k * 4 ) ] = state_a[ 0 ] ^ state_a[ 2 ] ^ state_a[ 3 ] ^ state_b[ 1 ] ^ state_b[ 2 ];
        state[ 2 + ( k * 4 ) ] = state_a[ 0 ] ^ state_a[ 1 ] ^ state_a[ 3 ] ^ state_b[ 2 ] ^ state_b[ 3 ];
        state[ 3 + ( k * 4 ) ] = state_a[ 0 ] ^ state_a[ 1 ] ^ state_a[ 2 ] ^ state_b[ 0 ] ^ state_b[ 3 ];
    }
}

uint8_t gmul( uint8_t a, uint8_t b )
{
    uint8_t p = 0;
    uint8_t h_bit;
    for ( size_t i = 0; i < 8; i++ )
    {
        if ( ( b & 1 ) == 1 )
        {
            p ^= a;
        }
        h_bit = ( a & 0x80 );
        a <<= 1;
        if ( h_bit == 0x80 )
        {
            a ^= 0x1b;
        }
        b >>= 1;
    }

    return p;
}

void invMixColumns( uint8_t* state )
{
    uint8_t a[ 4 ];

    for ( size_t k = 0; k < 4; k++ )
    {
        memcpy( a, state + k * 4, 4 );

        state[ 0 + k * 4 ] = gmul( a[ 0 ], 14 ) ^ gmul( a[ 3 ], 9 ) ^ gmul( a[ 2 ], 13 ) ^ gmul( a[ 1 ], 11 );
        state[ 1 + k * 4 ] = gmul( a[ 1 ], 14 ) ^ gmul( a[ 0 ], 9 ) ^ gmul( a[ 3 ], 13 ) ^ gmul( a[ 2 ], 11 );
        state[ 2 + k * 4 ] = gmul( a[ 2 ], 14 ) ^ gmul( a[ 1 ], 9 ) ^ gmul( a[ 0 ], 13 ) ^ gmul( a[ 3 ], 11 );
        state[ 3 + k * 4 ] = gmul( a[ 3 ], 14 ) ^ gmul( a[ 2 ], 9 ) ^ gmul( a[ 1 ], 13 ) ^ gmul( a[ 0 ], 11 );
    }
}

uint8_t rcon( size_t iteration )
{
    uint8_t rcon = 0x8d;

    for ( size_t i = 0; i < iteration; i++ )
    {
        rcon = ( ( rcon << 1 ) ^ ( 0x11b & -( rcon >> 7 ) ) ) & 0xff;
    }

    return rcon;
}

void keyScheduleCore( uint8_t* input, size_t iteration )
{
    rotWord( input );

    for ( size_t i = 0; i < 4; i++ )
    {
        input[ i ] = sbox[ input[ i ] ];
    }

    input[ 0 ] ^= rcon( iteration );
}

void msfsl::Cpu::expandKeysEncrypt( uint8_t* out_keys, uint8_t* in_key, size_t key_size )
{
    parseKeySize( key_size );
    size_t iteration = 1;
    size_t generated = key_size;
    size_t expanded_size = getExpandedSize( key_size );

    memcpy( out_keys, in_key, key_size );
    for ( size_t i = key_size; i < expanded_size; i += key_size )
    {
        memcpy( out_keys + generated, out_keys + generated - 4, 4 );
        keyScheduleCore( out_keys + generated, iteration );
        for ( size_t k = 0; k < 4; k++ )
        {
            out_keys[ generated + k ] ^= out_keys[ generated + k - key_size ];
        }
        iteration++;
        generated += 4;

        for ( size_t m = 0; m < ( key_size == 24 && generated < expanded_size ? 5 : 3 ); m++ )
        {
            memcpy( out_keys + generated, out_keys + generated - 4, 4 );
            out_keys[ generated + 0 ] ^= out_keys[ generated + 0 - key_size ];
            out_keys[ generated + 1 ] ^= out_keys[ generated + 1 - key_size ];
            out_keys[ generated + 2 ] ^= out_keys[ generated + 2 - key_size ];
            out_keys[ generated + 3 ] ^= out_keys[ generated + 3 - key_size ];
            generated += 4;
        }

        if ( ( key_size == 32 || key_size == 256 ) && generated < expanded_size )
        {
            memcpy( out_keys + generated, out_keys + generated - 4, 4 );
            for ( size_t k = 0; k < 4; k++ )
            {
                out_keys[ generated + k ] = sbox[ out_keys[ generated + k ] ];
                out_keys[ generated + k ] ^= out_keys[ generated + k - key_size ];
            }

            generated += 4;

            for ( size_t m = 0; m < 3; m++ )
            {
                memcpy( out_keys + generated, out_keys + generated - 4, 4 );
                out_keys[ generated + 0 ] ^= out_keys[ generated + 0 - key_size ];
                out_keys[ generated + 1 ] ^= out_keys[ generated + 1 - key_size ];
                out_keys[ generated + 2 ] ^= out_keys[ generated + 2 - key_size ];
                out_keys[ generated + 3 ] ^= out_keys[ generated + 3 - key_size ];
                generated += 4;
            }
        }
    }
}

void msfsl::Cpu::expandKeysDecrypt( uint8_t* out_keys, uint8_t* in_key, size_t key_size )
{
    expandKeysEncrypt( out_keys, in_key, key_size );

    size_t rounds = getRounds( key_size );
    uint8_t tmp[ AES_BLOCK_SIZE * rounds + AES_BLOCK_SIZE ];

    memcpy( tmp, out_keys, AES_BLOCK_SIZE * rounds + AES_BLOCK_SIZE );
    memcpy( out_keys, tmp + AES_BLOCK_SIZE * rounds, AES_BLOCK_SIZE );
    out_keys += AES_BLOCK_SIZE;
    for ( size_t i = 1; i < rounds; i++ )
    {
        memcpy( out_keys, tmp + AES_BLOCK_SIZE * ( rounds - i ), AES_BLOCK_SIZE );
        invMixColumns( out_keys );
        out_keys += AES_BLOCK_SIZE;
    }
    memcpy( out_keys, tmp, AES_BLOCK_SIZE );
}

void msfsl::Cpu::ecbEncrypt( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );
    int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );

    for ( size_t j = 0; j < blocks; j++ )
    {
        memcpy( cipher_text, plain_text, AES_BLOCK_SIZE );

        addRoundKey( cipher_text, keys );

        for ( size_t i = 1; i < rounds; i++ )
        {
            subBytes( cipher_text );
            shiftRows( cipher_text );
            mixColumns( cipher_text );
            addRoundKey( cipher_text, keys + AES_BLOCK_SIZE * i );
        }

        subBytes( cipher_text );
        shiftRows( cipher_text );
        addRoundKey( cipher_text, keys + AES_BLOCK_SIZE * rounds );

        plain_text += AES_BLOCK_SIZE;
        cipher_text += AES_BLOCK_SIZE;
    }
}

void msfsl::Cpu::ecbDecrypt( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );
    int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );

    for ( size_t j = 0; j < blocks; j++ )
    {
        memcpy( plain_text, cipher_text, AES_BLOCK_SIZE );

        addRoundKey( plain_text, keys );
        invShiftRows( plain_text );
        invSubBytes( plain_text );

        for ( size_t i = 1; i < rounds; i++ )
        {
            invMixColumns( plain_text );
            addRoundKey( plain_text, keys + AES_BLOCK_SIZE * i );
            invShiftRows( plain_text );
            invSubBytes( plain_text );
        }

        addRoundKey( plain_text, keys + AES_BLOCK_SIZE * rounds );

        plain_text += AES_BLOCK_SIZE;
        cipher_text += AES_BLOCK_SIZE;
    }
}

void msfsl::Cpu::ecbEncryptx86( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );
    int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );

#ifdef __linux__
    __m128i* k = ( __m128i* ) keys;
#elif __WIN32__
    __m128i k[ rounds + 1 ];
    for ( int i = 0; i < rounds + 1; i++ )
    {
        k[ i ] = _mm_loadu_si128( ( __m128i* ) keys );
        keys += AES_BLOCK_SIZE;
    }
#endif

    for ( size_t j = 0; j < blocks; j++ )
    {
        aesBlockEncryptx86( cipher_text, plain_text, k, text_size, key_size );

        plain_text += AES_BLOCK_SIZE;
        cipher_text += AES_BLOCK_SIZE;
    }
}

void msfsl::Cpu::ecbDecryptx86( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );
    int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );

#ifdef __linux__
    __m128i* k = ( __m128i* ) keys;
#elif __WIN32__
    __m128i k[ rounds + 1 ];
    for ( int i = 0; i < rounds + 1; i++ )
    {
        k[ i ] = _mm_loadu_si128( ( __m128i* ) keys );
        keys += AES_BLOCK_SIZE;
    }
#endif

    for ( size_t j = 0; j < blocks; j++ )
    {
        aesBlockDecryptx86( plain_text, cipher_text, k, text_size, key_size );

        plain_text += AES_BLOCK_SIZE;
        cipher_text += AES_BLOCK_SIZE;
    }
}

void msfsl::Cpu::ecbEncryptx86Multi( uint8_t* cipher_text, uint8_t* plain_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    try
    {
        int rounds = getRounds( key_size );
        int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );
        int blocks_per_thread = CEIL_DIV( blocks, NUM_THREADS );
        int text_size_per_thread = CEIL_DIV( text_size, NUM_THREADS );

        std::thread threads[ NUM_THREADS ];

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ] = std::thread( ecbEncryptx86, ( cipher_text + text_size_per_thread * i ), ( plain_text + text_size_per_thread * i ), keys, text_size_per_thread, key_size );
        }

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ].join();
        }
    }
    catch ( const std::exception& e )
    {
        std::cerr << e.what() << '\n';
    }
}

void msfsl::Cpu::ecbDecryptx86Multi( uint8_t* plain_text, uint8_t* cipher_text, uint8_t* keys, size_t text_size, size_t key_size )
{
    try
    {
        int rounds = getRounds( key_size );
        int blocks = CEIL_DIV( text_size, AES_BLOCK_SIZE );
        int blocks_per_thread = CEIL_DIV( blocks, NUM_THREADS );
        int text_size_per_thread = CEIL_DIV( text_size, NUM_THREADS );

        std::thread threads[ NUM_THREADS ];

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ] = std::thread( ecbDecryptx86, ( plain_text + text_size_per_thread * i ), ( cipher_text + text_size_per_thread * i ), keys, text_size_per_thread, key_size );
        }

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ].join();
        }
    }
    catch ( const std::exception& e )
    {
        std::cerr << e.what() << '\n';
    }
}