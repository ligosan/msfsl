#include "CpuAesCtr.h"
#include "CpuAesEcb.h"
#include <cassert>
#include <iostream>

#include "Common.h"

#define NUM_THREADS 32
#define AES_BLOCK_SIZE 16
#define CEIL_DIV( a, b ) ( ( a + b - 1 ) / b )

void ctrCryptx86Func( uint8_t* out_text, const uint8_t* in_text, uint8_t* keys, const std::vector<uint8_t> iv, size_t text_size, size_t key_size, size_t thread_idx = 0 )
{
    int rounds = msfsl::Cpu::getRounds( key_size );
    int blocks = text_size / AES_BLOCK_SIZE;

    size_t len = text_size;

    __uint128_t* iv_128 = ( __uint128_t* ) iv.data();

    iv_128[ 0 ] += ( thread_idx * blocks );

    __m128i* k = loadKeysx86( keys );

    while ( len >= AES_BLOCK_SIZE )
    {
        aesBlockEncryptx86( out_text, ( uint8_t* ) iv_128, k, text_size, key_size );

        // XOR
        for ( int i = 0; i < AES_BLOCK_SIZE; i++ )
        {
            out_text[ i ] ^= in_text[ i ];
        }

        in_text += AES_BLOCK_SIZE;
        out_text += AES_BLOCK_SIZE;
        len -= AES_BLOCK_SIZE;

        // TODO wrong increment
        iv_128[ 0 ]++;
    }
}

void msfsl::Cpu::ctrCryptx86( uint8_t* out_text, const uint8_t* in_text, uint8_t* keys, const std::vector<uint8_t> iv, const size_t text_size, const size_t key_size )
{
    int rounds = getRounds( key_size );
    int blocks = text_size / AES_BLOCK_SIZE;

    size_t len_remainder = text_size % AES_BLOCK_SIZE;
    uint8_t out_tmp[ AES_BLOCK_SIZE ];

    __uint128_t* iv_128 = ( __uint128_t* ) iv.data();

    __m128i* k = loadKeysx86( keys );

    ctrCryptx86Func( out_text, in_text, keys, iv, text_size, key_size );

    if ( len_remainder > 0 )
    {
        out_text += ( text_size - len_remainder );
        in_text += ( text_size - len_remainder );

        iv_128[ 0 ] += blocks;

        aesBlockEncryptx86( out_tmp, ( uint8_t* ) iv_128, k, text_size, key_size );

        // XOR
        for ( int i = 0; i < len_remainder; i++ )
        {
            out_text[ i ] = ( in_text[ i ] ^ out_tmp[ i ] );
        }
    }
}

void msfsl::Cpu::ctrCryptx86Multi( uint8_t* out_text, uint8_t* in_text, uint8_t* keys, const std::vector<uint8_t> iv, size_t text_size, size_t key_size )
{
    int rounds = getRounds( key_size );
    size_t len_remainder = text_size % AES_BLOCK_SIZE;
    std::vector<uint8_t> tmp( AES_BLOCK_SIZE );

    __uint128_t* iv_128 = ( __uint128_t* ) iv.data();

    try
    {
        int rounds = getRounds( key_size );
        int blocks = text_size / AES_BLOCK_SIZE;
        int blocks_per_thread = blocks / NUM_THREADS;
        int text_size_per_thread = blocks_per_thread * AES_BLOCK_SIZE;

        // TODO unaligned block sizes, e.g. 17 bytes
        // TODO odd block sizes dont work, e.g. 5 blocks, 11 blocks
        if ( ( text_size / NUM_THREADS ) < AES_BLOCK_SIZE || text_size != 0 && ( text_size & ( text_size - 1 ) ) != 0 )
        {
            // std::cout << "Using single threaded compute: " << text_size << std::endl;
            ctrCryptx86( out_text, in_text, keys, iv, text_size, key_size );
            return;
        }

        std::cout << "Using multi threaded compute: " << text_size << std::endl;

        std::thread threads[ NUM_THREADS ];

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ] = std::thread( ctrCryptx86Func, ( out_text + text_size_per_thread * i ), ( in_text + text_size_per_thread * i ), keys, iv, text_size_per_thread, key_size, i );
        }

        for ( int i = 0; i < NUM_THREADS; i++ )
        {
            threads[ i ].join();
        }

        if ( len_remainder > 0 )
        {
            auto offset = text_size - len_remainder;

            in_text += offset;
            out_text += offset;

            __m128i* k = loadKeysx86( keys );

            iv_128[ 0 ] += blocks;

            aesBlockEncryptx86( tmp.data(), ( uint8_t* ) iv_128, k, text_size, key_size );

            // XOR
            for ( int i = 0; i < len_remainder; i++ )
            {
                out_text[ i ] = ( in_text[ i ] ^ tmp[ i ] );
            }
        }
    }
    catch ( const std::exception& e )
    {
        std::cerr << e.what() << '\n';
    }
}