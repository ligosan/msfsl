
## Dependencies
### Linux
* Boost Library (1.58.0.1ubuntu1)

### Windows
The only real dependency you need is MinGW and CMake. All other required dependencies are packaged with the repo. But if you wish to compile and install your own version of the dependencies, then please ensure you follow the instruction below.

##### CMake
* Download and install the latest version of CMake from https://cmake.org/download/

##### MinGW
* Download and install the latest version of MinGW from https://sourceforge.net/projects/mingw/files/
* Install mingw32-base and mingw32-gcc-g++ packaes from the MinGW Installation Manager

##### OpenSSL
* Download latest version of OpenSSL binaries from https://slproweb.com/products/Win32OpenSSL.html
* Ensure Win32 version is downloaded

Please add root of the OpenSSL directory to PATH as "SSL_ROOT"

##### Google Test
* Download latest Google Test from https://github.com/google/googletest
* Open Powershell or Command Prompt in Administrator mode
* Compile and install with MinGW
```
mkdir build
cd build
cmake -G "MinGW Makefiles" ..
mingw32-make
mingw32-make install
```
Please add root of the Google Test to PATH as "GTEST_ROOT"

## Build
### Linux
```
mkdir build
cd build
cmake ..
make
```

### Windows
```
mkdir build
cd build
cmake -G "MinGW Makefiles" ..
mingw32-make
```


Good FFT Polynomial multiplication resources:
https://github.com/scipr-lab/libfqfft
https://math.stackexchange.com/questions/764727/concrete-fft-polynomial-multiplication-example
https://web.cecs.pdx.edu/~maier/cs584/Lectures/lect07b-11-MG.pdf
https://medium.com/@aiswaryamathur/understanding-fast-fourier-transform-from-scratch-to-solve-polynomial-multiplication-8018d511162f
