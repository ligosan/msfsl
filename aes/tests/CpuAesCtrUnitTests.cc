#include "Aes.h"
#include "openssl/aes.h"
#include "gtest/gtest.h"

#define TEST_ROUNDS 1

#define KB *1024
#define MB KB * 1024
#define GB MB * 1024

class CpuAesUnitTests : public ::testing::TestWithParam<int>
{
protected:
    AES_KEY ans_key;

    std::vector<uint8_t> iv;
    uint8_t* key;
    uint8_t* expanded_keys;
    uint8_t* reference_keys;

    uint8_t* cipher_text;
    uint8_t* ans_ctext;
    uint8_t* plain_text;

    std::vector<uint8_t> copy_text;

    size_t iv_size;

    // Key length in bytes
    size_t key_size;
    // Expanded key length in bytes
    size_t expanded_key_size;
    // Number of rounds in encryption
    size_t rounds;

    size_t plain_text_len;
    size_t block_count;
    size_t full_text_len;

    void init( size_t text_len = AES_BLOCK_SIZE )
    {
        plain_text_len = text_len;
        block_count = ( plain_text_len + AES_BLOCK_SIZE - 1 ) / AES_BLOCK_SIZE;
        full_text_len = block_count * AES_BLOCK_SIZE;
        key_size = GetParam() / 8;
        iv_size = AES_BLOCK_SIZE;

        switch ( key_size )
        {
        case 16:
        case 128:
            this->rounds = 11;
            break;
        case 24:
        case 192:
            this->rounds = 13;
            break;
        case 32:
        case 256:
            this->rounds = 15;
            break;
        }

        expanded_key_size = AES_BLOCK_SIZE * rounds;

        iv = std::vector<uint8_t>( AES_BLOCK_SIZE );
        copy_text = std::vector<uint8_t>( plain_text_len );
        key = new uint8_t[ key_size ];
        expanded_keys = new uint8_t[ expanded_key_size ];
        cipher_text = new uint8_t[ plain_text_len ];
        ans_ctext = new uint8_t[ plain_text_len ];
        plain_text = new uint8_t[ plain_text_len ];
    }

    void destroy()
    {
        copy_text.erase( copy_text.begin(), copy_text.end() );

        delete[] key;
        delete[] expanded_keys;
        delete[] cipher_text;
        delete[] ans_ctext;
        delete[] plain_text;
    }

    void SetUp()
    {
        // init();
    }

    void TearDown()
    {
        // destroy();
    }

    void fillRandomData( uint8_t* buffer, size_t buffer_size, size_t count = 1 )
    {
        for ( size_t i = 0; i < buffer_size * count; i++ )
        {
            buffer[ i ] = rand();
        }
    }

    void expandKeysEncrypt()
    {
        msfsl::Cpu::expandKeysEncrypt( expanded_keys, key, key_size );
    }

    void expandKeysDecrypt()
    {
        msfsl::Cpu::expandKeysDecrypt( expanded_keys, key, key_size );
    }

    void ctrCryptx86( uint8_t* out_data, uint8_t* in_data )
    {
        msfsl::Cpu::ctrCryptx86( out_data, in_data, expanded_keys, iv, plain_text_len, key_size );
    }

    void ctrCryptx86Multi( uint8_t* out_data, uint8_t* in_data )
    {
        msfsl::Cpu::ctrCryptx86Multi( out_data, in_data, expanded_keys, iv, plain_text_len, key_size );
    }

    void expandKeysEncRef()
    {
        AES_set_encrypt_key( key, key_size * 8, &ans_key );
    }

    void expandKeysDecRef()
    {
        AES_set_decrypt_key( key, key_size * 8, &ans_key );
    }

    void ctrEncryptAnswer()
    {
        for ( int i = 0; i < block_count; i++ )
        {
            // AES_encrypt_ctr( plain_text + AES_BLOCK_SIZE * i, ans_ctext + AES_BLOCK_SIZE * i, &ans_key );
        }
    }

    void printKeys( uint8_t* key, size_t key_size, size_t rounds )
    {
        for ( int i = 0; i < rounds; i++ )
        {
            for ( int k = 0; k < key_size; k++ )
            {
                printf( "%02x", key[ k + i * key_size ] );
            }
            printf( "\n" );
        }
    }
};

TEST_P( CpuAesUnitTests, CtrEncryptx86Multi )
{
    for ( int i = 0; i < 1 KB; i++ )
    {
        // std::cout << i << std::endl;
        init( i );
        fillRandomData( iv.data(), iv_size );
        fillRandomData( key, key_size );
        fillRandomData( plain_text, plain_text_len );

        expandKeysEncrypt();
        expandKeysEncRef();

        ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

        ctrCryptx86Multi( cipher_text, plain_text );
        ctrCryptx86Multi( copy_text.data(), cipher_text );

        ASSERT_TRUE( memcmp( copy_text.data(), plain_text, plain_text_len ) == 0 );
        destroy();
    }
}

TEST_P( CpuAesUnitTests, CtrEncryptx86 )
{
    for ( int i = 0; i < 1 KB; i++ )
    {
        // std::cout << i << std::endl;
        init( i );
        fillRandomData( iv.data(), iv_size );
        fillRandomData( key, key_size );
        fillRandomData( plain_text, plain_text_len );

        expandKeysEncrypt();
        expandKeysEncRef();

        ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

        ctrCryptx86( cipher_text, plain_text );
        ctrCryptx86( copy_text.data(), cipher_text );

        ASSERT_TRUE( memcmp( copy_text.data(), plain_text, plain_text_len ) == 0 );
        destroy();
    }
}

INSTANTIATE_TEST_CASE_P( CpuAesUnitTests, CpuAesUnitTests, ::testing::Values( 128, 192, 256 ) );

int main( int argc, char* argv[] )
{
    srand( time( 0 ) );
    ::testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS();
}
