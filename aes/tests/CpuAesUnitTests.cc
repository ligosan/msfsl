#include "Aes.h"
#include "openssl/aes.h"
#include "gtest/gtest.h"

#define TEST_ROUNDS 1

#define KB *1024
#define MB KB * 1024
#define GB MB * 1024

class CpuAesUnitTests : public ::testing::TestWithParam<int>
{
protected:
    AES_KEY ans_key;

    uint8_t* iv;
    uint8_t* key;
    uint8_t* expanded_keys;
    uint8_t* reference_keys;

    uint8_t* cipher_text;
    uint8_t* ans_ctext;
    uint8_t* plain_text;

    size_t iv_size;

    // Key length in bytes
    size_t key_size;
    // Expanded key length in bytes
    size_t expanded_key_size;
    // Number of rounds in encryption
    size_t rounds;

    size_t plain_text_len;
    size_t block_count;
    size_t full_text_len;

    void SetUp()
    {
        plain_text_len = 10 KB;
        block_count = ( plain_text_len + AES_BLOCK_SIZE - 1 ) / AES_BLOCK_SIZE;
        full_text_len = block_count * AES_BLOCK_SIZE;
        key_size = GetParam() / 8;
        iv_size = AES_BLOCK_SIZE;

        switch ( key_size )
        {
        case 16:
        case 128:
            this->rounds = 11;
            break;
        case 24:
        case 192:
            this->rounds = 13;
            break;
        case 32:
        case 256:
            this->rounds = 15;
            break;
        }

        expanded_key_size = AES_BLOCK_SIZE * rounds;

        iv = new uint8_t[ iv_size ];
        key = new uint8_t[ key_size ];
        expanded_keys = new uint8_t[ expanded_key_size ];
        cipher_text = new uint8_t[ full_text_len ];
        ans_ctext = new uint8_t[ full_text_len ];
        plain_text = new uint8_t[ full_text_len ];
    }

    void TearDown()
    {
        delete[] key;
        delete[] expanded_keys;
        delete[] cipher_text;
        delete[] ans_ctext;
        delete[] plain_text;
    }

    void fillRandomData( uint8_t* buffer, size_t buffer_size, size_t count = 1 )
    {
        for ( size_t i = 0; i < buffer_size * count; i++ )
        {
            buffer[ i ] = rand();
        }
    }

    void expandKeysEncrypt()
    {
        msfsl::Cpu::expandKeysEncrypt( expanded_keys, key, key_size );
    }

    void expandKeysDecrypt()
    {
        msfsl::Cpu::expandKeysDecrypt( expanded_keys, key, key_size );
    }

    void ecbEncrypt()
    {
        msfsl::Cpu::ecbEncrypt( cipher_text, plain_text, expanded_keys, plain_text_len, key_size );
    }

    void ecbEncryptx86Multi()
    {
        msfsl::Cpu::ecbEncryptx86Multi( cipher_text, plain_text, expanded_keys, plain_text_len, key_size );
    }

    void ecbDecryptx86Multi()
    {
        msfsl::Cpu::ecbDecryptx86Multi( plain_text, cipher_text, expanded_keys, plain_text_len, key_size );
    }

    void ecbEncryptx86()
    {
        msfsl::Cpu::ecbEncryptx86( cipher_text, plain_text, expanded_keys, plain_text_len, key_size );
    }

    void ecbDecryptx86()
    {
        msfsl::Cpu::ecbDecryptx86( plain_text, cipher_text, expanded_keys, plain_text_len, key_size );
    }

    void ecbDecrypt()
    {
        msfsl::Cpu::ecbDecrypt( plain_text, cipher_text, expanded_keys, plain_text_len, key_size );
    }

    void expandKeysEncRef()
    {
        AES_set_encrypt_key( key, key_size * 8, &ans_key );
    }

    void expandKeysDecRef()
    {
        AES_set_decrypt_key( key, key_size * 8, &ans_key );
    }

    void ecbEncryptAnswer()
    {
        for ( int i = 0; i < block_count; i++ )
        {
            AES_encrypt( plain_text + AES_BLOCK_SIZE * i, ans_ctext + AES_BLOCK_SIZE * i, &ans_key );
        }
    }

    void ecbDecryptAnswer()
    {
        for ( int i = 0; i < block_count; i++ )
        {
            AES_decrypt( cipher_text + AES_BLOCK_SIZE * i, ans_ctext + AES_BLOCK_SIZE * i, &ans_key );
        }
    }

    void printKeys( uint8_t* key, size_t key_size, size_t rounds )
    {
        for ( int i = 0; i < rounds; i++ )
        {
            for ( int k = 0; k < key_size; k++ )
            {
                printf( "%02x", key[ k + i * key_size ] );
            }
            printf( "\n" );
        }
    }
};

TEST_P( CpuAesUnitTests, EcbEncryptx86Multi )
{
    fillRandomData( key, key_size );
    fillRandomData( plain_text, plain_text_len );

    expandKeysEncrypt();
    expandKeysEncRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbEncryptx86Multi();
    ecbEncryptAnswer();

    ASSERT_TRUE( memcmp( cipher_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, EcbDecryptx86Multi )
{
    fillRandomData( key, key_size );
    fillRandomData( cipher_text, plain_text_len );

    expandKeysDecrypt();
    expandKeysDecRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbDecryptx86Multi();
    ecbDecryptAnswer();

    ASSERT_TRUE( memcmp( plain_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, EcbDecryptx86 )
{
    fillRandomData( key, key_size );
    fillRandomData( cipher_text, plain_text_len );

    expandKeysDecrypt();
    expandKeysDecRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbDecryptx86();
    ecbDecryptAnswer();

    ASSERT_TRUE( memcmp( plain_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, EcbEncryptx86 )
{
    fillRandomData( key, key_size );
    fillRandomData( plain_text, plain_text_len );

    expandKeysEncrypt();
    expandKeysEncRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbEncryptx86();
    ecbEncryptAnswer();

    ASSERT_TRUE( memcmp( cipher_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, EcbDecrypt )
{
    fillRandomData( key, key_size );
    fillRandomData( cipher_text, plain_text_len );

    expandKeysDecrypt();
    expandKeysDecRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbDecrypt();
    ecbDecryptAnswer();

    ASSERT_TRUE( memcmp( plain_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, EcbEncrypt )
{
    fillRandomData( key, key_size );
    fillRandomData( plain_text, plain_text_len );

    expandKeysEncrypt();
    expandKeysEncRef();

    ASSERT_TRUE( memcmp( ans_key.rd_key, expanded_keys, expanded_key_size ) == 0 );

    ecbEncrypt();
    ecbEncryptAnswer();

    ASSERT_TRUE( memcmp( cipher_text, ans_ctext, plain_text_len ) == 0 );
}

TEST_P( CpuAesUnitTests, KeyScheduleEncrypt )
{
    fillRandomData( key, key_size );

    expandKeysEncrypt();
    expandKeysEncRef();

    ASSERT_TRUE( memcmp( expanded_keys, ans_key.rd_key, expanded_key_size ) == 0 );
}

TEST_P( CpuAesUnitTests, KeyScheduleDecrypt )
{
    fillRandomData( key, key_size );

    expandKeysDecrypt();
    expandKeysDecRef();

    ASSERT_TRUE( memcmp( expanded_keys, ans_key.rd_key, expanded_key_size ) == 0 );
}

INSTANTIATE_TEST_CASE_P( CpuAesUnitTests, CpuAesUnitTests, ::testing::Values( 128, 192, 256 ) );

int main( int argc, char* argv[] )
{
    srand( time( 0 ) );
    ::testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS();
}
