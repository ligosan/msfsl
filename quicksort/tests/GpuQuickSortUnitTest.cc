#include <gtest/gtest.h>
#include "GpuQuickSortKernel.cuh"

TEST(GPU, JoinList){
}

TEST(GPU, AddList){
}

TEST(GPU, FunctionalTest){

}

TEST(CPU, JoinList){
}

TEST(CPU, AddList){
}

TEST(CPU, FunctionalTest){
	int size = 13;
	int expectedList[ size ] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15};
	int list[ size ] = {5, 4, 3, 2, 1, 10, 7, 11, 15, 12, 8, 6, 9};
	int* newList = new int[ size ];

	QSK::splitList(list, newList, size);

	for(int i = 0; i < size; i++){
		ASSERT_EQ(expectedList[i], newList[i]);
	}

	delete[] newList;
}

int main(int argc, char* argv[]){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
