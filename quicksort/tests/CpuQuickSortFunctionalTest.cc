#include <iostream>
#include <gtest/gtest.h>
#include <ctime>
#include <cstdio>

#include "CpuQuickSort.h"

void expectedSort(int* list, int size){
	int comparisons = 0;
	int temp;
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size - 1; j++){
			if(list[j] > list[j+1]){
				temp = list[j];
				list[j] = list[j+1];
				list[j+1] = temp;
			}
			comparisons++;
		}
	}
	
/*
	std::cout << "Comparisons: " << comparisons << std::endl;
	std::cout << "Size: " << size << std::endl;
*/
}

void CpuQuickSortFunctionalTest(int size){
	int* myList = new int[size];
	int* newList = new int[size];
	int* expectedList = new int[size];	


	std::clock_t start;
	double duration;

	QSC::initList(myList, size);
	QSC::copyList(myList, expectedList, size);

	//start = std::clock();
	expectedSort(expectedList, size);
	//duration = (std::clock() - start)/(double) CLOCKS_PER_SEC;
	//std::cout << "Expected: " << duration << std::endl;

	QSC::copyList(myList, newList, size);

	//start = std::clock();
	QSC::splitList(myList, newList, size);
	//duration = (std::clock() - start)/(double) CLOCKS_PER_SEC;
	//std::cout << "QuickSort: " << duration << std::endl;
	
	for(int i = 0; i < size; i++){
		ASSERT_EQ(newList[i], expectedList[i]);
	}

	delete[] myList;
	delete[] newList;
	delete[] expectedList;
}

TEST(CpuQuickSort, FunctionalTest){
	for(int i = 1; i <= 10; i++){
		CpuQuickSortFunctionalTest(i * 1000);
	}
}

int main(int argc, char* argv[]){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
