#include "GpuQuickSortKernel.cuh"

TEST(GPU, FunctionalTest){
	for(int i = 0; i <= 10; i++){
		int size = i * 1000;
		int* list = new int[size];
		int* newList = new int[size];

		QSK::initList(list, size);
		//displayList(list, size, 0);
		QSK::splitList(list, newList, size);
		//displayList(newList, size, 1);
		expectedSort(list, size);

		for(int j = 0; j < size; j++){
			ASSERT_EQ(newList[j], list[j]);
		}

		delete[] list;
		delete[] newList;
	}
}

int main(int argc, char* argv[]){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}