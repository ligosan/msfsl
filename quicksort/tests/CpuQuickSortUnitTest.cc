#include <gtest/gtest.h>
#include "CpuQuickSort.h"

TEST(GPU, JoinList){
}

TEST(CPU, JoinList){
}

TEST(CPU, FunctionalTest){
	int list[10] = {5, 4, 3, 2, 1, 10, 7, 8, 6, 9};
	int expectedList[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int* newList = new int[10];

	QSC::splitList(list, newList, 10);

	for(int i = 0; i < 10; i++){
		ASSERT_EQ(expectedList[i], newList[i]);
	}

	delete[] newList;
}

int main(int argc, char* argv[]){
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
