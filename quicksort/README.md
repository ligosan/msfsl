Quick Sort C++
==============

This is the C++ implementation of the quick sort algorithm. It has both the serial and the parallelised version.