#ifndef QUICK_SORT_KERNEL_H
#define QUICK_SORT_KERNEL_H
namespace QSK{
	void displayList(int* myList, int size, int type);
	int getSize(int* list, int listSize, int middleNumber, int type);
	void splitList(int* list, int* newList, int size);
	void initList(int* list, int size);
}
#endif
