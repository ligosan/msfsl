#ifndef QUICK_SORT_H
#define QUICK_SORT_H
namespace QSC{
	void displayList(int* myList, int size, int type);
	void joinList(int* lowerList, int* upperList, int* newList, int lowerSize, int upperSize, int listSize, int middleNumber);
	void addList(int* list, int listSize, int* newList, int middleNumber, int type);
	int getSize(int* list, int listSize, int middleNumber, int type);
	void splitList(int* list, int* newList, int size);
	void initList(int* list, int size);
	void copyList(int* copyFrom, int* copyTo, int size);
}
#endif
