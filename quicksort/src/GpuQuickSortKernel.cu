#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "GpuQuickSortKernel.cuh"

/**********************************************************************************
 * C++ implementation of the Quick Sort algorithm.
 * This is the parallel GPU version.
 **********************************************************************************/
namespace QSK{
/*
	void displayList(int* myList, int size, int type){
		std::cout << "List " << type << ": ";
		for(int i = 0; i < size; i++){
			std::cout << myList[i];
			if(i == size - 1){
				std::cout << std::endl;
			}
			else{
				std::cout << ", ";
			}
		}
	}
*/
	__device__ void copyList(int* copyFrom, int* copyTo, int size){
		int index = blockDim.x * blockIdx.x + threadIdx.x;

		if(index < size){
			copyTo[index] = copyFrom[index];
		}
	}

	__global__ void joinList(int* lowerList, int* upperList, int* newList, int lowerSize, int upperSize, int listSize, int middleNumber){
		int* tempList = new int[listSize];

		copyList(lowerList, tempList, lowerSize);
		tempList[lowerSize] = middleNumber;

		for(int i = 0; i < upperSize; i++){
			tempList[i + lowerSize + 1] = upperList[i];
		}

		copyList(tempList, newList, listSize);
	
		delete[] tempList;
	}

	__global__ void addList(int* list, int listSize, int* newList, int middleNumber, int type){
		int counter = 0;
		//int index = blockDim.x * blockIdx.x + threadIdx.x;
	
		if(type == 0){
	/*
			if((index + 1) < listSize){
				if(list[index] < middleNumber || list[index] == middleNumber){
					newList[counter] = list[index];
					counter++;
				}
			}
	*/
			for(int i = 1; i < listSize; i++){
				if(list[i] < middleNumber || list[i] == middleNumber){
					newList[counter] = list[i];
					counter++;
				}
			}

		}
		else if(type == 1){
			for(int i = 1; i < listSize; i++){
				if(list[i] > middleNumber){
					newList[counter] = list[i];
					counter++;
				}
			}
		}
	}

	int getSize(int* list, int listSize, int middleNumber, int type){
		int size = 0;
	
		// For lower numbers
		if(type == 0){
			for(int i = 1; i < listSize; i++){
				if(list[i] < middleNumber || list[i] == middleNumber){
					size++;
				}
			}
		} 
		// For upper numbers
		else if (type == 1){
			for(int i = 1; i < listSize; i++){
				if(list[i] > middleNumber){
					size++;
				}
			}
		}
	
		return size;
	}
	void splitList(int* list, int* newList, int size){

		int middleNumber = list[0];
		int lowerCount = getSize(list, size, middleNumber, 0);
		int upperCount = getSize(list, size, middleNumber, 1);

		int* lowerList = new int[lowerCount];
		int* upperList = new int[upperCount];

		int* kernelList;
		int* kernelLowerList;
		int* kernelUpperList;
		int* kernelNewList;

		cudaMalloc((int**) &kernelList, sizeof(int) * size);
		cudaMalloc((int**) &kernelLowerList, sizeof(int) * lowerCount);	
		cudaMalloc((int**) &kernelUpperList, sizeof(int) * upperCount);
		cudaMalloc((int**) &kernelNewList, sizeof(int) * size);

		cudaMemcpy(kernelList, list, sizeof(int) * size, cudaMemcpyHostToDevice);

		addList<<<size/512 + 1, 512>>>(kernelList, size, kernelLowerList, middleNumber, 0);
		addList<<<size/512 + 1, 512>>>(kernelList, size, kernelUpperList, middleNumber, 1);

		cudaMemcpy(list, kernelList, sizeof(int) * size, cudaMemcpyDeviceToHost);
		cudaMemcpy(lowerList, kernelLowerList, sizeof(int) * lowerCount, cudaMemcpyDeviceToHost);
		cudaMemcpy(upperList, kernelUpperList, sizeof(int) * upperCount, cudaMemcpyDeviceToHost);

		if(size > 1){	
			splitList(lowerList, lowerList, lowerCount);
			splitList(upperList, upperList, upperCount);
		}
	

		cudaMemcpy(kernelList, list, sizeof(int) * size, cudaMemcpyHostToDevice);
		cudaMemcpy(kernelLowerList, lowerList, sizeof(int) * lowerCount, cudaMemcpyHostToDevice);
		cudaMemcpy(kernelUpperList, upperList, sizeof(int) * upperCount, cudaMemcpyHostToDevice);

		joinList<<<upperCount/512 + 1, 512>>>(kernelLowerList, kernelUpperList, kernelNewList, lowerCount, upperCount, size, middleNumber);
		cudaMemcpy(newList, kernelNewList, sizeof(int) * size, cudaMemcpyDeviceToHost);
	
		delete[] lowerList;
		delete[] upperList;
		cudaFree(kernelList);
		cudaFree(kernelNewList);
		cudaFree(kernelLowerList);
		cudaFree(kernelUpperList);
	}

	void initList(int* list, int size){
		srand(time(NULL));
	
		for(int i = 0; i < size; i++){
			list[i] = rand() % size + 1;
		}
	}
}