#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "CpuQuickSort.h"

/**********************************************************************************
 * C++ implementation of the Quick Sort algorithm.
 * This is the serial CPU version.
 **********************************************************************************/
namespace QSC{
	void displayList(int* myList, int size, int type){
		std::cout << "List " << type << ": ";
		for(int i = 0; i < size; i++){
			std::cout << myList[i];
			if(i == size - 1){
				std::cout << std::endl;
			}
			else{
				std::cout << ", ";
			}
		}
	}

	void copyList(int* copyFrom, int* copyTo, int size){
		for(int i = 0; i < size; i++){
			copyTo[i] = copyFrom[i];
		}
	}

	void joinList(int* lowerList, int* upperList, int* newList, int lowerSize, int upperSize, int listSize, int middleNumber){
		int* tempList = new int[listSize];
	
		copyList(lowerList, tempList, lowerSize);
		tempList[lowerSize] = middleNumber;
		for(int i = 0; i < upperSize; i++){
			tempList[i + lowerSize + 1] = upperList[i];
		}
		copyList(tempList, newList, listSize);
	
		delete[] tempList;
	}

	void addList(int* list, int listSize, int* newList, int middleNumber, int type){
		int counter = 0;
	
		if(type == 0){
			for(int i = 1; i < listSize; i++){
				if(list[i] < middleNumber || list[i] == middleNumber){
					newList[counter] = list[i];
					counter++;
				}
			}
		}
		else if(type == 1){
			for(int i = 1; i < listSize; i++){
				if(list[i] > middleNumber){
					newList[counter] = list[i];
					counter++;
				}
			}
		}
	}

	int getSize(int* list, int listSize, int middleNumber, int type){
		int size = 0;
	
		// For lower numbers
		if(type == 0){
			for(int i = 1; i < listSize; i++){
				if(list[i] < middleNumber || list[i] == middleNumber){
					size++;
				}
			}
		} 
		// For upper numbers
		else if (type == 1){
			for(int i = 1; i < listSize; i++){
				if(list[i] > middleNumber){
					size++;
				}
			}
		}
	
		return size;
	}
	void splitList(int* list, int* newList, int size){

		int middleNumber = list[0];
		int lowerCount = getSize(list, size, middleNumber, 0);
		int upperCount = getSize(list, size, middleNumber, 1);

		int* lowerList = new int[lowerCount];
		int* upperList = new int[upperCount];
	
		addList(list, size, lowerList, middleNumber, 0);
		addList(list, size, upperList, middleNumber, 1);
	
		if(size > 1){	
			splitList(lowerList, lowerList, lowerCount);
			splitList(upperList, upperList, upperCount);
		}

		joinList(lowerList, upperList, newList, lowerCount, upperCount, size, middleNumber);
	
		delete[] lowerList;
		delete[] upperList;
	}

	void initList(int* list, int size){
		srand(time(NULL));
	
		for(int i = 0; i < size; i++){
			list[i] = rand() % size + 1;
		}
	}
}
