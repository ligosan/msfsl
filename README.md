# MSFSL #

Just a collection of libraries that I wrote out of boredom... CUDA supported of course :D

### TODO ###
* Bitsliced AES
* Reed Solomon ECC
* AES over FFT
* Reed Solomon over FFT
* Burrows Wheeler
* LZW
* AES-NI hardware level intrinsic (potential backdoor?)

### Algorithms ###

#### Compression ####
* LZ78

#### Sorting ####
* Quick sort

#### Resiliency ####
* Blockchain

#### Cryptography ####

### Contributors ###

* Michael Jang
