#include "Utils.h"

using namespace std;

class Node
{
private:
    uint32_t key_;
	Node* parent_;
    int8_t value_;
    vector< Node > child_nodes_;
    
public:
    /**
     * Default constructor
     */
    Node();
	
	/**
     * Constructor to setup the initial node
     */
    Node( uint32_t key, uint8_t value );
    
    /**
     * Constructor to setup the initial node
     */
    Node( uint32_t key, uint8_t value, Node* parent_ );
    
    /**
     * Get the key value of the currently selected node
     */
    uint32_t getKey();
    
    /**
     * Get the value of the byte of the currently selected node
     */
    int8_t getValue();
    Node* getChildByValue( int8_t value );
    Node* getChildByKey( uint32_t key );
    
    /**
     * Adds a child node
     */
    void addChild( Node node );
    
    /**
     * Clears all child nodes
     */
    void clearChildNodes();
    
    void printAllChildNodes();
	
	void traversePrint( uint32_t key );
};
