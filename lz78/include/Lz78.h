#include "Node.h"

// User level
namespace Lz78
{
	/**
	 * Decodes using LZ78 Algorithm
	 */
	void decode( const char* file_name );
	
	/**
	 * Encodes using LZ78 Algorithm
	 */
	void encode( const char* file_name );
};