#include "Lz78.h"

// Maximum number of bits to use to represent the node key for each node
#define MAX_BIT 16

void fillDictionary( char* data_buf, uint64_t data_buf_size, uint64_t max_trie_entries )
{
    Node head_node( 0, 0 );
    Node* tmp_node = nullptr;
    tmp_node = (Node*)&head_node;
    uint32_t trie_counter = 1;
    uint32_t parent_key = 0;
    
    // TODO discard tree when filled and start a new one
    for( size_t i = 0; i < data_buf_size; i++ )
    {
		//printf( "%c\n", data_buf[ i ] );
        // If our dictionary reaches the maximum number of entries allowed, then create a new dictionary
        if( trie_counter >= max_trie_entries )
        {
            head_node.clearChildNodes();
            tmp_node = (Node*)&head_node;
            trie_counter = 1;
            
            // Print the new dictionary marker
            cout << "$$$$$" << endl;
        }
        // See if the value of the character exists within the dictionary
        else if( tmp_node->getChildByValue( data_buf[ i ] ) != NULL )
        {
            // If we do have the character, then set that Node as the head pointer
            tmp_node = tmp_node->getChildByValue( data_buf[ i ] );
            parent_key = tmp_node->getKey();
            
			if( ( i + 1 ) >= data_buf_size )
			{
				cout << parent_key << "," << EOF << endl;
			}
			
			//printf( "EYE: %lu buff: %lu\n", i, data_buf_size );
			continue;
        }
        
        // Add the node to the dictionary
        Node node( trie_counter, data_buf[ i ] );
        tmp_node->addChild( node );
        
        // Output the value
        cout << parent_key << "," << node.getValue() << endl;
        
        // Increment the trie counter and reset the tmp_node to point back to the head node, also reset parent key
        trie_counter++;
        tmp_node = (Node*)&head_node;
        parent_key = 0;
    }
	
	//head_node.printAllChildNodes();
}

void Lz78::decode( const char* file_name )
{
    ifstream input_file;
    input_file.open( file_name );
    
    string line;
    
    Node head_node( 0, 0 );
    Node* tmp_node;
    uint32_t trie_counter = 1;
    
    while( getline( input_file, line ) )
    {	
		//cout << line << std::endl;
        // Extract the parent key from the string
        string tmp;
        tmp = line.substr( 0, line.find( ',' ) );
        
        // Set the parent key and value of the current node to add
        uint32_t parent_key = std::atoi( tmp.c_str() );
        int8_t value = (int8_t)( line.substr( line.find( ',' ) ) ).at( 1 );
		
		//cout << line << " " << trie_counter << " " << value << std::endl;
        
        // Construct a new node with the values extracted
        Node child_node( trie_counter, value, head_node.getChildByKey( parent_key ) );
        
        // If the child can be added directly beneath the root node
        if( parent_key == 0 )
        {
            head_node.addChild( child_node );
            trie_counter++;
            
            cout << value;
        }
        // Otherwise search and find the correct place to add the node
        else
        {
            tmp_node = head_node.getChildByKey( parent_key );
			
            if( tmp_node != NULL )
            {
				tmp_node->addChild( child_node );
				child_node.traversePrint( trie_counter );
				
				trie_counter++;
            }
        }
    }
	
	//printf( "\n" );
	
	//head_node.printAllChildNodes();
}

void Lz78::encode( const char* file_name )
{
    ifstream input_file;
    input_file.open( file_name, std::ifstream::binary );
    
    // Extract the file length
    input_file.seekg( 0, input_file.end );
    uint64_t file_size = input_file.tellg();
    input_file.seekg( 0, input_file.beg );
    
    uint64_t max_trie_entries = pow( 2, MAX_BIT );
    
    // TODO cast issue perhaps?
    char* data_buf = new char[ file_size ];
    
	// TODO merge this part with filling in the dictionary.. 
	// To avoid running out of memory when processing large files
    for( size_t i = 0; i < file_size; i++ )
    {
        data_buf[ i ] = input_file.peek();
        input_file.seekg( 1 + i, input_file.beg );
		//printf( "%c\n", data_buf[ i ] );
    }
    
    input_file.close();
    
    // Fill the dictionary and output results
    fillDictionary( data_buf, file_size, max_trie_entries );
	
	delete[] data_buf;
}