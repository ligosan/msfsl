#include "Node.h"

Node::Node()
{
}

Node::Node( uint32_t key, uint8_t value )
{
    this->key_ = key;
    this->value_ = value;
}

Node::Node( uint32_t key, uint8_t value, Node* parent )
{
    this->key_ = key;
    this->value_ = value;
	this->parent_ = parent;
}

uint32_t Node::getKey()
{
    return this->key_;
}

int8_t Node::getValue()
{
    return this->value_;
}

void Node::addChild( Node node )
{
    this->child_nodes_.push_back( node );
}

Node* Node::getChildByValue( int8_t value )
{
    for( size_t i = 0; i < child_nodes_.size(); i++ )
    {
        // If a match is found
        if( child_nodes_.at( i ).getValue() == value )
        {
            return &( child_nodes_.at( i ) );
        }
    }
	
    return NULL;
}

void Node::traversePrint( uint32_t key )
{
	if( this->parent_->getKey() == 0 )
	{
		printf( "%c", this->value_ );
		return;
	}
	
	parent_->traversePrint( key );
	printf( "%c", this->value_ );
}

Node* Node::getChildByKey( uint32_t key )
{
	if( this->key_ == key )
	{
		return this;
	}
	
    for( size_t i = 0; i < child_nodes_.size(); i++ )
    {
		auto t = child_nodes_.at( i ).getChildByKey( key );
		if( t != NULL )
		{
			return t;
		}
    }
	
	return NULL;
}

void Node::clearChildNodes()
{
    this->child_nodes_.clear();
}

void Node::printAllChildNodes()
{
    for( size_t i = 0; i < child_nodes_.size(); i++ )
    {
        printf( "Key: %u Value: %c\n", child_nodes_.at( i ).getKey(), child_nodes_.at( i ).getValue() );
		child_nodes_.at( i ).printAllChildNodes();
    }
}
