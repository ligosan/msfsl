#include "CpuBlockchain.h"
#include <iostream>
#include <ctime>
#include <cstring>

namespace msfsl
{
	char* getTimeStamp()
	{
		// Variable to hold current time in
		std::time_t raw_time;
		
		// Get current time
		std::time( &raw_time );
		// Converting current time to a human readable string
		char* time_stamp = std::asctime( std::localtime( &raw_time ) );
		
		return time_stamp;
	}
	
	Cpu::Blockchain::Blockchain()
	{
		uint8_t data[ 32 ];
		uint8_t prev_hash[ SHA1_LENGTH ];
		memset( data, 0, 32 );
		
		// Genesis block has no previous hash
		memset( prev_hash, 0, SHA1_LENGTH );
		
		Cpu::Block genesis_block( 0, getTimeStamp(), prev_hash, (uint8_t*)&data, 32 );

		this->block_chain.push_back( genesis_block );
	}

	void Cpu::Blockchain::addBlock( uint8_t* data, size_t data_size )
	{
		uint64_t block_id = this->block_chain.size();

		uint8_t* prev_hash = block_chain.at( block_id - 1 ).getHash();
		Cpu::Block new_block( block_id, getTimeStamp(), prev_hash, data, data_size );
		
		this->block_chain.push_back( new_block );
	}
	
	void Cpu::Blockchain::print()
	{
		for( auto b : block_chain )
		{
			b.print();
		}
	}
}
