#include "CpuBlock.h"

namespace msfsl
{
Cpu::Block::Block( uint64_t block_id, char* time_stamp, uint8_t* previous_hash, uint8_t* data, size_t data_size )
{
	this->block_id_ = block_id;
	this->time_stamp_ = time_stamp;
	
	memcpy( previous_hash_, previous_hash, SHA1_LENGTH );
	
	this->data_ = data;
	this->data_size_ = data_size;

	if( data != NULL )
	calculateHash( (uint8_t*)&this->current_hash_, this->data_, this->data_size_ );
}

uint8_t* Cpu::Block::getHash()
{
	return this->current_hash_;
}

void Cpu::Block::calculateHash( uint8_t* out, uint8_t* in, size_t in_size )
{
	SHA1(in, in_size, out);
}

void printHash( uint8_t* hash )
{
	for( size_t i = 0; i < SHA1_LENGTH; i++ )
	{
		printf( "%02x", hash[ i ] );
	}
}

void Cpu::Block::print()
{
	std::cout << "{" << std::endl;
	
	std::cout << "	block_id      : " << this->block_id_ << std::endl;
	std::cout << "	time_stamp    : " << this->time_stamp_ << std::endl;
	
	std::cout << "	previous_hash : ";
	printHash( this->previous_hash_ );
	std::cout << std::endl;
	
	std::cout << "	current_hash  : ";
	printHash( this->current_hash_ );
	std::cout << std::endl;
	
	std::cout << "}" << std::endl;
}
}
