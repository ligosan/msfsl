#include <vector>

#include "CpuBlock.h"

namespace msfsl
{
namespace Cpu
{		
class Blockchain
{
public:
	Blockchain();
	/* Adds a block to the chain */
	void addBlock( uint8_t* data, size_t data_size );
	
	void print();

private:
	std::vector< msfsl::Cpu::Block > block_chain;
};
}
}
