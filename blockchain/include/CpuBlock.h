#include <cstdint>
#include <iostream>
#include <cstring>

#include "openssl/sha.h"

namespace msfsl
{
	
#define SHA1_LENGTH 20
	
namespace Cpu
{
class Block
{
public:
	/* Constructor */
	Block( uint64_t block_id, char* time_stamp, uint8_t* previous_hash, uint8_t* data, size_t data_size );
	
	/* Calculate the hash of the current block */
	void calculateHash( uint8_t* out, uint8_t* in, size_t in_size );
	
	uint8_t* getHash();

	void print();
	
private:
	/* Index ID of the block */
	uint64_t block_id_;
	/* Randomly generated nonce */
	uint64_t nonce_;
	
	/* Stamp of the time and date when this block was created */
	char* time_stamp_;
	
	/* Hash of the previous block */
	uint8_t previous_hash_[ SHA1_LENGTH ];
	/* Hash of this block */
	uint8_t current_hash_[ SHA1_LENGTH ];
	/* The data that is stored in this block */
	uint8_t* data_;

	size_t data_size_;
};
}
}
