#include "CpuBlockchain.h"
#include "openssl/sha.h"
#include <ctime>
#include <cstring>
#include <ctime>

using namespace std;

void getRandData( uint8_t* data, size_t data_size )
{
	for( size_t i = 0; i < data_size; i++ )
	{
		data[ i ] = rand();
	}
}

int main( int argc, char* argv[] )
{
	srand( time( NULL ) );
	
	msfsl::Cpu::Blockchain bc;

	uint8_t data[ 16 ];

	memset( data, 0, 16 );

	bc.addBlock( (uint8_t*)&data, 16 );
	
	getRandData( (uint8_t*)&data, 16 );
	bc.addBlock( (uint8_t*)&data, 16 );
	
	getRandData( (uint8_t*)&data, 16 );
	bc.addBlock( (uint8_t*)&data, 16 );
	
	getRandData( (uint8_t*)&data, 16 );
	bc.addBlock( (uint8_t*)&data, 16 );
	
	bc.print();
}
